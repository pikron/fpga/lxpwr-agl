GHDL=ghdl
SRC=util.vhdl crc8.vhdl cmladc.vhdl mcpwm.vhdl sioreg.vhdl lxpwr.vhdl \
	sioreg_tb.vhdl sioreg_xch2_tb.vhdl sioreg_chain_tb.vhdl \
	lxpraso1master.vhdl lxpraso1master_tb.vhdl

#SIM_FLAGS=--stop-time=1000ns #--ieee-asserts=disable

all:	test-sioreg

test-lxprw: analyze-all
	$(GHDL) -m lxpwr

test-sioreg: analyze-all
	$(GHDL) -m sioreg_tb
	$(GHDL) -r sioreg_tb --stop-time=1000ns --vcd=$@.vcd

test-sioreg-xch2: analyze-all
	$(GHDL) -m sioreg_xch2_tb
	$(GHDL) -r sioreg_xch2_tb --stop-time=1000ns --vcd=$@.vcd

test-sioreg-chain: analyze-all
	$(GHDL) -m sioreg_chain_tb
	$(GHDL) -r sioreg_chain_tb --stop-time=1000ns --vcd=$@.vcd

test-lxpraso1master: analyze-all
	$(GHDL) -m lxpraso1master_tb
	$(GHDL) -r lxpraso1master_tb --stop-time=40000ns --vcd=$@.vcd

analyze-all: $(SRC)
	$(GHDL) -a $(SRC)


clean:
	$(GHDL) --clean
	$(RM) work-obj93.cf