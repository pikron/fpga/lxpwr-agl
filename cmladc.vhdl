--
-- * sigma-integral ADC digital part *
-- ADC described in:
--  PISA, P.; PORAZIL, P. Suma-Integration Analog to Digital
--  Converter, Idea, Im-plementation and Results. In HORACEK, P.
--  SIMANDL, M. ZITEK, P. (Ed.) Preprints of the 16th World Congress
--  of the International Federation of Automatic Control, s. 1 6,
--  Prague, July 2005. IFAC.
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cmladc is
  generic (
    width: natural := 16
  );
  port (
    -- reset: in std_logic;
    clock: in std_logic;
    sync: in std_logic;
    count: in std_logic_vector (width-1 downto 0);
    value: out std_logic_vector (width-1 downto 0);
    adc_mux: out std_logic;
    adc_cmp: in std_logic
  );    
end cmladc;

architecture behavioral of cmladc is
  signal q, last_q, next_q, edge: std_logic;
  signal value_reg, next_value_reg: std_logic_vector (value'range);
begin
  value <= value_reg;
  adc_mux <= not q;
  
  edge <= q and not last_q;
  next_value_reg <= count when edge = '1' else value_reg;
  
  rs: process (sync, adc_cmp, q)
  begin
    next_q <= q;
    --
    -- assume sync is a 1-clock pulse
    -- prioritize reset (=sync) over set (=adc_cmp)
    --
    if sync = '1' then
      next_q <= '0';
    elsif adc_cmp = '0' then
      next_q <= '1';
    end if;
  end process;

--   --next_q <= not ( sync or (q and adc_cmp));
--   next_q <= (adc_cmp or q) and not sync;
  
  seq: process
  begin
    wait until clock'event and clock = '1';
    value_reg <= next_value_reg;
    q <= next_q;
    last_q <= q;
  end process;
end behavioral;
