LXPWR protocol

Data worlds are transferred in LSB sent first format.

The PWM worlds contain two control bits starting
from world MSB side and 14 bit PWM

  bit 15  en_n
  bit 14  en_p

  bits 13 .. 0  PWM

Actual function of en_n and en_p bits is connected to the used
output stages half-bridges driver circuit.

For LT1158 base chips output equations are

  out_p <= q and en_p_reg and not failsafe;
  out_n <= en_n_reg or failsafe;

which gives next function to the enable/disable bits

  en_n .. if set or there is fault then power stage is switched off,
          i.e. output is disconnected from power and ground rail,
  en_p .. enables PWM output for given output

The input data packet starts by activation of sync signal (logic
level 0) and shift register content is transferred into PWM comparators
inputs after sync signal deactivation (logic 1). Received data consists of
  8 x 16 bit en_p+en_n+PWM worlds
  1 x 8 bit CRC computed from previous data worlds

The output shift register is filled by ADC measured slope times
at the time of deactivation of sync signal and then data are sent
bit by bit to the master including 8 bit CRC.


The data are captured and changed by rising edge of the clock signal.
The propagation delay from clock signal to the time when new data
are available on the output pin and delay of the next chip input
logic ensures for synchronous clock that this setup is safe.

But there can be problem in the case of delays differences
between ISO7240M channels.

The required delay between data clock rising edge and corresponding
data line change can be solved by TCL Actel Command Designer
or on Xilinx side for Xilinx to AGL side
(repair min time violation).

The AGL delay outputs delay (about 8 ns to clock rising edge) on data
lines is big enough to ensure correct signals ordering.

Comands to synthetize the design

  synplify_pro -licensetype synplifypro_actel -batch syn.tcl

  designer SCRIPT:par.tcl LOGFILE:par.log
