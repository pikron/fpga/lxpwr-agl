--
-- * SIPO/PISO register chain *
--  together with CRC8 checksum
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 10/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity sioreg is
  generic (
    n: natural := 32
  );
  port (
    clock      : in std_logic;
    sync       : in std_logic;
    sin        : in std_logic;
    sout       : out std_logic;
    start_in   : in std_logic;
    start_out  : out std_logic;
    data_valid,
    data_error : out std_logic;
    data_in    : in std_logic_vector (n-1 downto 0);
    data_out   : out std_logic_vector (n-1 downto 0)
  );
end sioreg;

architecture behavioral of sioreg is
  component crc is
    port (
      reset, clock, init, inp_bit: in std_logic;
      out_crc: out std_logic_vector (7 downto 0)
    );
  end component;
  --
  type state_t is (ST_XFER, ST_CRCOUT_PUSH,
                      ST_CRCOUT_SHIFT, ST_CHECK, ST_END);
  constant crc_len: natural := 8;
  constant count_nb: natural := ceil_log2(n);
  constant cst_count_nb: natural := ceil_log2(n+crc_len+1);
  signal cycle_count, next_cycle_count: natural range 0 to 2**count_nb-1;
  signal cst_count, next_cst_count: natural range 0 to 2**cst_count_nb-1;
  signal crc_in_reg, next_crc_in_reg, crc_out_reg, next_crc_out_reg,
    crc_in_calc, crc_out_calc: std_logic_vector (crc_len-1 downto 0);
  signal data_reg, next_data_reg: std_logic_vector (n-1 downto 0);
  signal crc_in_init, crc_out_init, out_mux: std_logic;
  signal st, next_st: state_t;
begin
  crc_in: crc
    port map (
      reset => '0',                     -- unused
      clock => clock,
      init => crc_in_init,
      inp_bit => crc_in_reg(0),
      out_crc => crc_in_calc
    );
  crc_out: crc
    port map (
      reset => '0',                     -- unused
      clock => clock,
      init => crc_out_init,
      inp_bit => data_reg(0),
      out_crc => crc_out_calc
    );
  --
  crc_out_init <= sync;
  
  next_crc_in_reg <= sin & crc_in_reg(crc_in_reg'high downto 1);
  next_data_reg <=
    crc_in_reg(0) & data_reg(n-1 downto 1) when sync = '0'
    else data_in;
  sout <= data_reg(0) when out_mux = '0' else crc_out_reg(0);

  -- inherent 1-clock shift necessary
  data_out <= crc_in_reg(0) & data_reg(n-1 downto 1);
  
  next_crc_out_reg <=
    data_reg(0) & crc_out_reg(crc_out_reg'high downto 1) when out_mux = '1'
    else crc_out_calc;

  crc_start: process (start_in, cst_count)
  begin
    crc_in_init <= '0';
    start_out <= '0';
    --
    next_cst_count <= (cst_count + 1) mod 2**cst_count_nb;
    --
    -- If the cst_count is allowed to continue run when
    -- shift register is fully filled then sioreg
    -- can receive next following frames until sync
    -- does not confirm that the last received frame
    -- belongs to this chip
    if cst_count = 0 then
      next_cst_count <= 0;
    elsif cst_count = crc_in_reg'length then
      crc_in_init <= '1';
    elsif cst_count = n+crc_len then
      start_out <= '1';
      next_cst_count <= 1;  -- changed from 0 to 1
    end if;
    --
    if start_in = '1' then
      next_cst_count <= 1;
    end if;
  end process;

  check: process (sync, sin, crc_in_reg, crc_in_calc)
  begin
    data_valid <= '0';
    data_error <= '0';
    --
    -- this *SLOW* algebraic dependence on input can be
    -- replaced by faster one, abandoning MSB of CRC
    --
    if sync = '1' then
      if sin & crc_in_reg(crc_in_reg'high downto 1) = crc_in_calc then
        data_valid <= '1';
      else
        data_error <= '1';
      end if;
    end if;
  end process;
  
  fsm: process (st, cycle_count, sync)
  begin
    out_mux <= '0';
    next_st <= st;
    next_cycle_count <= (cycle_count + 1) mod 2**count_nb;
    case st is
      when ST_XFER =>
        if cycle_count >= n-1 then
          next_st <= ST_CRCOUT_SHIFT;
          next_cycle_count <= 0;
        end if;

      when ST_CRCOUT_SHIFT =>
        out_mux <= '1';
        if cycle_count >= crc_in_reg'high then
          next_st <= ST_END;
        end if;

      when ST_END =>
        out_mux <= '0';

      when others =>
        -- nihil
    end case;
    --
    if sync = '1' then
      next_st <= ST_XFER;
      next_cycle_count <= 0;
    end if;
  end process;
  
  seq: process
  begin
    wait until clock'event and clock = '1';
    crc_in_reg <= next_crc_in_reg;
    data_reg <= next_data_reg;
    crc_out_reg <= next_crc_out_reg;
    st <= next_st;
    cycle_count <= next_cycle_count;
    cst_count <= next_cst_count;
  end process;
end behavioral;
