library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sioreg_xch2_tb is
end sioreg_xch2_tb;

architecture test of sioreg_xch2_tb is
  component sioreg is
    generic (
      n: natural
      );
    port (
      clock      : in std_logic;
      sync       : in std_logic;
      sin        : in std_logic;
      sout       : out std_logic;
      start_in   : in std_logic;
      start_out  : out std_logic;
      data_valid,
      data_error : out std_logic;
      data_in    : in std_logic_vector (n-1 downto 0);
      data_out   : out std_logic_vector (n-1 downto 0)
      );    
  end component;
  --
  constant data_in_a: std_logic_vector :=
    b"1011_1010_1111";
  constant data_in_b: std_logic_vector :=
    b"1110_0001_1010";
  constant sync_sim: std_logic_vector :=
    b"0_1000_0000_0000_0000_0000_1000_0000_0000_0000_0000_10"
    & b"00_0000_0000_0000_0000_10"
    ;
  signal clock, sync, saibo, saobi: std_logic;
  signal t: natural := 0;
begin
  sioreg_a: sioreg
    generic map (
      n => data_in_a'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => saibo,
      sout     => saobi,
      start_in => sync,
      data_in  => data_in_a);
  sioreg_b: sioreg
    generic map (
      n => data_in_b'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => saobi,
      sout     => saibo,
      start_in => sync,
      data_in  => data_in_b);

  sim: process
  begin
    if t <= sync_sim'high then sync <= sync_sim(t); end if;
    wait until clock'event and clock = '1';
    t <= t + 1;
  end process;
  
  process
  begin
    clock <= '0';
    wait for 5 ns;
    clock <= '1';
    wait for 5 ns;
  end process;
end test;
