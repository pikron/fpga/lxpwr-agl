# synplify_pro -licensetype synplifypro_actel -batch syn.tcl

project -new lxpwr
impl -name syn0

add_file pll50to200.vhd
add_file siroladc.vhdl
add_file pdchain.vhdl
add_file pdivtwo.vhdl
add_file crc8.vhdl
add_file mcpwm.vhdl
add_file sioreg.vhdl
add_file util.vhdl

# top-level
add_file lxpwr.vhdl
add_file lxpwr.sdc

set_option -technology IGLOO
set_option -part AGL250V5
set_option -package VQFP100
set_option -speed_grade std

set_option -frequency 50.0

project -run
project -save
