--
-- * motion-control PWM *
--  PWM controller with failsafe input
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mcpwm is
  generic (
    pwm_width: natural := 12
  );
  port (
    -- reset: in std_logic;
    clock: in std_logic;
    sync, data_valid, failsafe: in std_logic;
    en_p, en_n: in std_logic;
    match: in std_logic_vector (pwm_width-1 downto 0);
    count: in std_logic_vector (pwm_width-1 downto 0);
    out_p, out_n: out std_logic
  );
end mcpwm;

architecture behavioral of mcpwm is
  signal match_reg, next_match_reg: std_logic_vector (match'range);
  signal en_p_reg, en_n_reg, next_en_p_reg, next_en_n_reg: std_logic;
  signal q, next_q: std_logic;
begin
  out_p <= q and en_p_reg and not failsafe;
  -- out_n <= not q and en_n_reg and not failsafe;
  out_n <= en_n_reg or failsafe;
  
  reg: process (data_valid, failsafe, match, match_reg,
                en_p, en_n, en_p_reg, en_n_reg)
  begin
    next_match_reg <= match_reg;
    next_en_p_reg <= en_p_reg;
    next_en_n_reg <= en_n_reg;
    if failsafe = '1' then
      --
      -- little paranoia, costs nothing
      --
      next_en_p_reg <= '0';
      next_en_n_reg <= '0';
    elsif data_valid = '1' then
      next_match_reg <= match;
      next_en_p_reg <= en_p;
      next_en_n_reg <= en_n;
    end if;
  end process;

  rs: process (sync, count, match_reg, q)
  begin
    if count = match_reg then
      next_q <= '0';
    elsif sync = '1' then
      next_q <= '1';
    else
      next_q <= q;
    end if;
  end process;

  seq: process
  begin
    wait until clock'event and clock = '1';
    match_reg <= next_match_reg;
    en_p_reg <= next_en_p_reg;
    en_n_reg <= next_en_n_reg;
    q <= next_q;
  end process;
end behavioral;
