library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sioreg_tb is
end sioreg_tb;

architecture test of sioreg_tb is
  component sioreg is
    generic (
      n: natural
      );
    port (
      clock      : in std_logic;
      sync       : in std_logic;
      sin        : in std_logic;
      sout       : out std_logic;
      start_in   : in std_logic;
      start_out  : out std_logic;
      data_valid,
      data_error : out std_logic;
      data_in    : in std_logic_vector (n-1 downto 0);
      data_out   : out std_logic_vector (n-1 downto 0)
      );    
  end component;
  --
  constant data_in: std_logic_vector :=
    b"1011_1010_1111";
  constant sync_sim: std_logic_vector :=
    b"01000000000000000000000000000010";
  constant sin_sim: std_logic_vector :=
    b"00_1110_0001_1010_00101101";
  signal clock, sync, sin: std_logic;
  signal t: natural := 0;
begin
  sioreg0: sioreg
    generic map (
      n => data_in'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => sin,
      start_in => sync,
      data_in  => data_in);

  sim: process
  begin
    if t <= sync_sim'high then sync <= sync_sim(t); end if;
    if t <= sin_sim'high then sin <= sin_sim(t); end if;
    wait until clock'event and clock = '1';
    t <= t + 1;
  end process;
  
  process
  begin
    clock <= '0';
    wait for 5 ns;
    clock <= '1';
    wait for 5 ns;
  end process;
end test;
