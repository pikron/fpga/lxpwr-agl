--
-- * LXPWR slave part *
--  common sioreg & common counter for several ADC&PWM blocks
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity lxpwr is
  generic (
    n_pwm: natural := 8;
    n_adc: natural := 9;
    n_cfg: natural := 2;
    pwm_width: natural := 14;
    adc_width: natural := 16;
    adc_cycle: natural := 125;
    adc_sjw: natural := 2;
    max_fail_count: natural := 3
    );
  port (
    clkm      : in std_logic;
    sync      : in std_logic;
    sin       : in std_logic;
    sout      : out std_logic;
    -- sg1i      : in std_logic;
    sg1q      : out std_logic;
    clks      : out std_logic;
    -- fail      : out std_logic;
    pwm_a,
    pwm_b     : out std_logic_vector (n_pwm-1 downto 0);
    adc_out   : out std_logic_vector (n_adc-1 downto 0);
    adc_in    : in std_logic_vector (n_adc-1 downto 0);
    hal_in    : in std_logic_vector (n_pwm-1 downto 0);
    fault_in  : in std_logic_vector (n_pwm-1 downto 0);
    cfg_in  : in std_logic_vector (n_cfg-1 downto 0)
    );
end lxpwr;

architecture behavioral of lxpwr is
  component sioreg is
    generic (
      n: natural
      );
    port (
      clock      : in std_logic;
      sync       : in std_logic;
      sin        : in std_logic;
      sout       : out std_logic;
      start_in   : in std_logic;
      start_out  : out std_logic;
      data_valid,
      data_error : out std_logic;
      data_in    : in std_logic_vector (n-1 downto 0);
      data_out   : out std_logic_vector (n-1 downto 0)
      );
  end component;
  component mcpwm is
    generic (
      pwm_width: natural
      );
    port (
      clock, sync, data_valid, failsafe: in std_logic;
      en_p, en_n: in std_logic;
      match, count: in std_logic_vector;
      out_p, out_n: out std_logic
      );
  end component;
  component siroladc is
    generic (
      width: natural
    );
    port (
      -- reset: in std_logic;
      clock: in std_logic;
      sync: in std_logic;
      pcounter_value: out std_logic_vector (width-1 downto 0);
      adc_mux: out std_logic;
      adc_cmp: in std_logic
    );
  end component;
  -- Actel lib
  component pll50to200
    port (
      powerdown, clka: in std_logic;
      lock, gla: out std_logic
    );
  end component;
  component CLKINT
    port (A: in std_logic; Y: out std_logic);
  end component;
  --
  constant sio_head_len: natural := 16;
  constant sio_adc_len: natural := 32; -- adc_width+2
  constant sio_pwm_len: natural := 16; -- pwm_width+2
  type pwm_matrix_t is array (0 to n_pwm-1) of
    std_logic_vector (sio_pwm_len-1 downto 0);
  type adc_matrix_t is array (0 to n_adc-1) of
    std_logic_vector (adc_width-1 downto 0);
  constant count_width: natural := max(pwm_width,adc_width);
  constant sioreg_len: natural := max(n_pwm*sio_pwm_len,n_adc*sio_adc_len) + sio_head_len;
  signal clock, pll_clkin, pll_clkout: std_logic;
  signal adc_data, pwm_data: std_logic_vector (sioreg_len-sio_head_len-1 downto 0);
  signal pwm_word: pwm_matrix_t;
  signal adc_word: adc_matrix_t;
  signal last_adc_word: adc_matrix_t;
  signal reset, pll_lock, sync_edge, sync_edge_reg, last_sync: std_logic;
  signal adc_cycle_cnt: integer range 0 to adc_cycle+adc_sjw-1;
  signal adc_sync, next_adc_sync: std_logic;
  signal sync2fast, last_sync2fast, next_sync2fast: std_logic;
  signal data_valid, data_error, failsafe, next_failsafe: std_logic;
  signal fail_count, next_fail_count: integer range 0 to max_fail_count;
  signal count_n, next_count_n: natural range 0 to 2**count_width-1;
  signal count: std_logic_vector (count_width-1 downto 0);
  signal hal_reg: std_logic_vector (n_pwm-1 downto 0);
  signal fault_reg: std_logic_vector (n_pwm-1 downto 0);
  signal sio_head_sent, sio_head_rec: std_logic_vector (sio_head_len-1 downto 0);
  signal clkmon_dly1, clkmon_dly2: std_logic;
  signal clkmon_fail, clkmon_fail_next: std_logic;
  signal clkmon_wdg: integer range 0 to 6;
  signal reset_sync, reset_async: std_logic;
begin
-- PLL as a reset generator
   copyclk: CLKINT
     port map (
       a => clkm,
       y => clock);
  pll: pll50to200
    port map (
      powerdown => '1',
      clka => pll_clkin,
      gla => pll_clkout,
      lock => pll_lock);

  reset_async <= not pll_lock or clkmon_fail;
                      -- TODO: apply reset for good failsafe
                      --       upon power-on

  pll_clkin <= clock;

  sioreg0: sioreg
    generic map (
      n => sioreg_len)
    port map (
      clock      => clock,
      sync       => sync_edge,
      sin        => sin,
      sout       => sout,
      start_in   => sync, -- sg1i,  -- DEBUG => sync for single slave only!
      start_out  => sg1q,
      data_valid => data_valid,
      data_error => data_error,
      data_in(sio_head_len-1 downto 0) => sio_head_sent,
      data_in(sioreg_len-1 downto sio_head_len) => adc_data,
      data_out(sio_head_len-1 downto 0) => sio_head_rec,
      data_out(sioreg_len-1 downto sio_head_len) => pwm_data);
  --
  pwm_block: for i in n_pwm-1 downto 0 generate
    pwm: mcpwm
      generic map (pwm_width => pwm_width)
      port map (
        clock => clock,
        sync => sync_edge_reg,
        data_valid => data_valid,
        failsafe => failsafe,
        --
        -- pwm config bits & match word
        --
        en_n => pwm_word(i)(pwm_width+1),
        en_p => pwm_word(i)(pwm_width),
        match => pwm_word(i)(pwm_width-1 downto 0),
        count => count(pwm_width-1 downto 0),
        -- outputs
        out_p => pwm_a(i),
        out_n => pwm_b(i)
        );
  end generate;
  pwm_conn: for i in 0 to n_pwm-1 generate
    pwm_word(i) <= pwm_data(i*sio_pwm_len + sio_pwm_len-1
                            downto i*sio_pwm_len);
  end generate;
  adc_block: for i in n_adc-1 downto 0 generate
    adc: siroladc
      generic map (width => adc_width)
      port map (
        clock => pll_clkout,
        sync => adc_sync,
        pcounter_value => adc_word(i)(adc_width-1 downto 0),
        adc_mux => adc_out(i),
        adc_cmp => adc_in(i)
      );
  end generate;
  adc_conn: for i in 0 to n_adc-1 generate
    adc_data(i*sio_adc_len + adc_width-1
             downto i*sio_adc_len) <= adc_word(i);
    adc_data(i*sio_adc_len + 2 * adc_width-3
             downto i*sio_adc_len + adc_width) <= last_adc_word(i)(adc_width-3 downto 0);
  end generate;
  hal_fault_conn: for i in 0 to n_pwm-1 generate
    adc_data(i*sio_adc_len + 2 * adc_width-2) <= hal_reg(i);
    adc_data(i*sio_adc_len + 2 * adc_width-1) <= fault_reg(i);
  end generate;

  adc_data((n_pwm+0)*sio_adc_len + 2 * adc_width-2) <= cfg_in(0);
  adc_data((n_pwm+0)*sio_adc_len + 2 * adc_width-1) <= cfg_in(1);

  sio_head_sent <= x"b1" & std_logic_vector(to_unsigned((sioreg_len-sio_head_len)/16, 8));

  -- silly(?) attempt to some degree of SSI/SPI compatibility
  sync_edge <= sync and not last_sync;

  failure: process (data_valid, data_error, failsafe, fail_count)
  begin
    next_failsafe <= failsafe;
    next_fail_count <= fail_count;
    if data_valid = '1' then
      next_failsafe <= '0';
      next_fail_count <= 0;
    elsif data_error = '1' then
      if fail_count = max_fail_count then
        next_failsafe <= '1';
      else
        next_fail_count <= fail_count + 1;
      end if;
    end if;
  end process;

  cnt: process (count_n, sync_edge)
  begin
    count <= std_logic_vector(to_unsigned(count_n, count'length));
    if sync_edge = '1' or count_n = 2**count_width-1 then
      next_count_n <= 0;
    else
      next_count_n <= count_n + 1;
    end if;
  end process;
  
  seq: process
  begin
    wait until clock'event and clock = '1';
    last_sync <= sync;
    fail_count <= next_fail_count;
    count_n <= next_count_n;
    for i in 0 to n_pwm-1 loop
      hal_reg(i) <= hal_in(i);
      fault_reg(i) <= fault_in(i);
    end loop;
  end process;

  async_rst: process (clock, reset_async, reset_sync)
  begin
    if reset_async = '1' then
      failsafe <= '1';
    elsif clock'event and clock = '1' then
      failsafe <= next_failsafe or reset_sync;
    end if;
  end process;

  sync_rst: process (clock, reset_async)
  begin
    if clock'event and clock = '1' then
      reset_sync <= reset_async;
    end if;
  end process;

  next_adc_sync <= not last_sync2fast and sync2fast;
  clks <= sync2fast;

  adc_last: process(clock, adc_word, sync_edge, reset_sync)
  begin
    if reset_sync = '1' then
      adc_cycle_cnt <= 0;
      next_sync2fast <= '0';
      last_adc_word <= ((others=> (others=>'0')));
      sync_edge_reg <= '0';
    elsif clock'event and clock = '1' then
      sync_edge_reg <= sync_edge;
      if sync_edge = '1' then
        adc_cycle_cnt <= adc_sjw;
        next_sync2fast <= '0';
      elsif adc_cycle_cnt = 0 then
        adc_cycle_cnt <= adc_cycle - 1;
        last_adc_word <= adc_word;
        next_sync2fast <= '1';
      else
        adc_cycle_cnt <= adc_cycle_cnt - 1;
        next_sync2fast <= '0';
      end if;
    end if;
  end process;

  fast_domain_sync: process (pll_clkout, reset_sync, sync2fast, next_sync2fast, next_adc_sync)
  begin
    if reset_sync = '1' then
      sync2fast <= '0';
    elsif pll_clkout'event and pll_clkout = '1' then
      sync2fast <= next_sync2fast;
      last_sync2fast <= sync2fast;
      adc_sync <= next_adc_sync;
    end if;
  end process;

  clock_monitor: process (pll_clkout, clock, clkmon_dly1, clkmon_wdg, clkmon_fail_next)
  begin
    if pll_clkout'event and pll_clkout = '1' then
      clkmon_dly1 <= clock;
      clkmon_dly2 <= clkmon_dly1;
      if clkmon_dly1 = '0' and clkmon_dly2 = '1' then
        clkmon_wdg <= 6;
        clkmon_fail_next <= '0';
      elsif clkmon_wdg > 0 then
        clkmon_wdg <= clkmon_wdg - 1;
        clkmon_fail_next <= '0';
      else
        clkmon_wdg <= 0;
        clkmon_fail_next <= '1';
      end if;
      clkmon_fail <= clkmon_fail_next;
    end if;
  end process;

end behavioral;
