# designer SCRIPT:par.tcl LOGFILE:par.log

# create a new design
new_design -name "lxpwr" -family "IGLOO"

set_device \
    -die AGL125V5 \
    -package "100 VQFP" \
    -speed STD \
    -voltage 1.5 \
    -iostd LVTTL \
    -jtag yes \
    -probe yes \
    -trst yes \
    -temprange COM \
    -voltrange COM

# set default back-annotation base-name
set_defvar "BA_NAME" "lxpwr_ba"

# set working directory
set_defvar "DESDIR" "par0"

# set back-annotation output directory
set_defvar "BA_DIR" "par0"

# enable the export back-annotation netlist
set_defvar "BA_NETLIST_ALSO" "1"

# setup status report options
set_defvar "EXPORT_STATUS_REPORT" "1"
set_defvar "EXPORT_STATUS_REPORT_FILENAME" "lxpwr.rpt"

# legacy audit-mode flags (left here for historical reasons)
set_defvar "AUDIT_NETLIST_FILE" "1"
set_defvar "AUDIT_DCF_FILE" "1"
set_defvar "AUDIT_PIN_FILE" "1"
set_defvar "AUDIT_ADL_FILE" "1"

# import of input files
import_source  \
-format "edif" -edif_flavor "GENERIC" -netlist_naming "VHDL" "syn0/lxpwr.edn" \
-format "pdc" "lxpwr.pdc"

# export translation of original netlist
export -format "vhdl" "_map.vhdl"

compile \
    -pdc_abort_on_error on \
    -pdc_eco_display_unmatched_objects off \
    -pdc_eco_max_warnings 10000 \
    -demote_globals off \
    -demote_globals_max_fanout 12 \
    -promote_globals off \
    -promote_globals_min_fanout 200 \
    -promote_globals_max_limit 0 \
    -localclock_max_shared_instances 12 \
    -localclock_buffer_tree_max_fanout 12 \
    -combine_register on \
    -delete_buffer_tree off \
    -delete_buffer_tree_max_fanout 12 \
    -report_high_fanout_nets_limit 10

# auxiliary source files
import_aux -format "sdc" "syn0/lxpwr_sdc.sdc"

save_design lxpwr.adb

layout \
    -timing_driven \
    -run_placer on \
    -place_incremental off \
    -run_router on \
    -route_incremental off \
    -placer_high_effort off

save_design lxpwr.adb

export \
    -format bts_stp \
    -feature prog_fpga \
    lxpwr.stp

save_design lxpwr.adb
