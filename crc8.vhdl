--
-- * CRC-8 generator *
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity crc is
  port (
    reset: in std_logic;
    clock: in std_logic;
    init: in std_logic;
    inp_bit: in std_logic;
    out_crc: out std_logic_vector (7 downto 0)
  );
end crc;

architecture behavioral of crc is
  signal reg, next_reg: std_logic_vector (out_crc'range);
begin
  out_crc <= next_reg;                  -- reg;

  comb: process (init, inp_bit, reg)
  begin
    if init = '1' then
      next_reg <= (others => '1');
    else
      next_reg(4 downto 0) <= reg(5 downto 1);
      next_reg(5) <= reg(0) xor reg(6);
      next_reg(6) <= reg(0) xor reg(7);
      next_reg(7) <= reg(0) xor inp_bit;
    end if;
  end process;

  seq: process
  begin
    wait until clock'event and clock = '1';
    reg <= next_reg;
  end process;
end behavioral;

