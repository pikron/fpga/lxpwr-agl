library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sioreg_chain_tb is
end sioreg_chain_tb;

architecture test of sioreg_chain_tb is
  component sioreg is
    generic (
      n: natural
      );
    port (
      clock      : in std_logic;
      sync       : in std_logic;
      sin        : in std_logic;
      sout       : out std_logic;
      start_in   : in std_logic;
      start_out  : out std_logic;
      data_valid,
      data_error : out std_logic;
      data_in    : in std_logic_vector (n-1 downto 0);
      data_out   : out std_logic_vector (n-1 downto 0)
      );    
  end component;
  --
  constant data_in_a0: std_logic_vector := x"123"; --"baf";
  constant data_in_a1: std_logic_vector := x"4567"; --"ded0";
  constant data_in_b0: std_logic_vector := x"987"; --"a1e";
  constant data_in_b1: std_logic_vector := x"6543"; --"bab1";
  constant sync_sim: std_logic_vector :=
    b"0_10";
  signal clock, sync, sa, sab, sb, sba, cst_a, cst_b: std_logic;
  signal t: natural := 0;
begin
  sioreg_a0: sioreg
    generic map (
      n => data_in_a0'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => sa,
      sout     => sab,
      start_in => cst_a,
      data_in  => data_in_a0);
  sioreg_a1: sioreg
    generic map (
      n => data_in_a1'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => sba,
      sout     => sa,
      start_in => sync,
      start_out => cst_a,
      data_in  => data_in_a1);
  sioreg_b0: sioreg
    generic map (
      n => data_in_b0'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => sb,
      sout     => sba,
      start_in => cst_b,
      data_in  => data_in_b0);
  sioreg_b1: sioreg
    generic map (
      n => data_in_b1'length)
    port map (
      clock    => clock,
      sync     => sync,
      sin      => sab,
      sout     => sb,
      start_in => sync,
      start_out => cst_b,
      data_in  => data_in_b1);  

  sim: process
  begin
    if t <= sync_sim'high then sync <= sync_sim(t); end if;
    wait until clock'event and clock = '1';
    t <= t + 1;
  end process;
  
  process
  begin
    clock <= '0';
    wait for 5 ns;
    clock <= '1';
    wait for 5 ns;
  end process;
end test;
