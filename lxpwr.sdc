
# create_clock -name name -period period_value [-waveform edge_list] source
create_clock -name {clock} -period 20 -waveform {0 10} {p:clkm}

#create_clock -name {pll_clkout} -period 5 -waveform {0 2.5} {n:pll_clkout}

# create_generated_clock -name {name -source reference_pin [-divide_by divide_factor] \
#   [-multiply_by multiply_factor] [-invert] source -pll_output pll_feedback_clock \
#   -pll_feedback pll_feedback_input

create_generated_clock -name {pll_clkout} \
  -multiply_by 4 \
  -source {n:clock} \
  {n:pll_clkout}

# set_false_path -through {n:clkmon_fail}
