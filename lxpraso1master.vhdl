library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lxpraso1master is
  port (
    clock     : in std_logic;
    clkm      : out std_logic;
    sync_out  : out std_logic;
    mosi      : out std_logic;
    miso      : in std_logic
    );
end lxpraso1master;

architecture rtl of lxpraso1master is
  component sioreg is
    generic (
      n: natural
      );
    port (
      clock      : in std_logic;
      sync       : in std_logic;
      sin        : in std_logic;
      sout       : out std_logic;
      start_in   : in std_logic;
      start_out  : out std_logic;
      data_valid,
      data_error : out std_logic;
      data_in    : in std_logic_vector (n-1 downto 0);
      data_out   : out std_logic_vector (n-1 downto 0)
      );    
  end component;
  --
  constant data_len: natural := 8*16;
  constant crc_len: natural := 8;
  constant frame_len: natural := 1024;
  signal pwm_ctl: std_logic_vector (data_len-1 downto 0);
  signal sync: std_logic;
  signal count, nx_count: natural range 0 to frame_len-1 := 0;
begin
  pwm_ctl <= x"c0cc_c199_c266_0333_83f0_0000_0000_0000";
  
  clkm <= clock;
  sync_out <= sync;

  sioreg0: sioreg
    generic map (
      n => data_len)
    port map (
      clock => clock,
      sync => sync,
      sin => miso,
      sout => mosi,
      start_in => sync,
      data_in => pwm_ctl);

  nx_count <= 0 when count = frame_len-1 else count + 1;
  sync <= '1' when count >= data_len+crc_len-1 else '0';
  
  process
  begin
    wait until clock'event and clock = '1';
    count <= nx_count;
  end process;
end rtl;
