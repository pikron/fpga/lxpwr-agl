library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lxpraso1master_tb is
end lxpraso1master_tb;

architecture test of lxpraso1master_tb is
  component lxpraso1master is
    port (
      clock     : in std_logic;
      clkm      : out std_logic;
      sync_out  : out std_logic;
      mosi      : out std_logic;
      miso      : in std_logic
      );
  end component;
  component lxpwr is
    generic (
      n_pwm: natural := 8;
      n_adc: natural := 8;
      pwm_width: natural := 14;
      adc_width: natural := 16;
      max_fail_count: natural := 3
      );
    port (
      clkm      : in std_logic;
      sync      : in std_logic;
      sin       : in std_logic;
      sout      : out std_logic;
      -- sg1i      : in std_logic;
      sg1q      : out std_logic;
      -- fail      : out std_logic;
      pwm_a,
      pwm_b     : out std_logic_vector (n_pwm-1 downto 0);
      adc_out   : out std_logic_vector (n_adc-1 downto 0);
      adc_in    : in std_logic_vector (n_adc-1 downto 0)
      );
  end component;
  --
  signal clock, clkm, sync, mosi, miso: std_logic;
  signal t: natural := 0;
begin
  m: lxpraso1master
    port map (
      clock    => clock,
      clkm     => clkm,
      sync_out => sync,
      mosi     => mosi,
      miso     => miso);
  s: lxpwr
    port map (
      clkm => clkm,
      sync => sync,
      sin  => mosi,
      sout => miso,
      adc_in => (others => '0'));

  process
  begin
    clock <= '0';
    wait for 5 ns;
    clock <= '1';
    wait for 5 ns;
  end process;
end test;
